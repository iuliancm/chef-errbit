#
# Cookbook:: install_apache
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

package 'mongodb' do
  action :install
  end

service 'mongodb' do
  action [ :enable, :start ]
  retries 3
end

package 'curl' do
  action :install
  end

script 'add_repos' do
  interpreter "bash"
  code <<-EOH
    curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  EOH
  end

execute "apt-update" do
  command "apt-get update"
  action :run
  end

package %w(git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn) do
  action :install
  end

script 'add_rbenv' do
  interpreter "bash"
  code <<-EOH
   cd
   git clone https://github.com/rbenv/rbenv.git ~/.rbenv
   echo 'export PATH="~/.rbenv/bin:$PATH"' >> ~/.bashrc
   echo 'eval "$(rbenv init -)"' >> ~/.bashrc
   chown -R $(stat -c %G:%U ~/.bashrc) ~/.rbenv
   git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
   echo 'export PATH="~/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
   chown -R $(stat -c %G:%U ~/.bashrc) ~/.rbenv/plugins/ruby-build
  EOH
  end

execute "install_ruby" do
  command "~/.rbenv/bin/rbenv install 2.7.6"
  action :run
  end

execute "global_ruby" do
  command "~/.rbenv/bin/rbenv global 2.7.6"
  action :run
  end

execute "install_bundler" do
  command "~/.rbenv/shims/gem install bundler"
  action :run
  end

#script 'install_ruby' do
#  interpreter "bash"
#  code <<-EOH
#  cd 
#   su -l $(stat -c %G ~/.bashrc)
#   rbenv install 2.7.6
#   rbenv global 2.7.6
#   gem install bundler
#  EOH
#  end
  
git "/opt/errbit" do
  repository "https://github.com/errbit/errbit.git"
  reference "main"
  action :sync
  end

execute "errbit_perms" do
  command "chown -R $(stat -c %G:%U ~/.bashrc) /opt/errbit"
  action :run
  end

execute "bundle_install" do
  command "cd /opt/errbit/ && ~/.rbenv/shims/bundle install"
  action :run
  end

execute "bootstrap_errbit" do
  command "~/.rbenv/shims/bundle exec rake errbit:bootstrap > /opt/errbit/bootstrap.out"
  cwd "/opt/errbit/"
  creates "/opt/errbit/bootstrap.out"
  end

script 'create_systemd_service' do
  interpreter "bash"
  code <<-EOH
  if [[ $(stat -c %u /opt/errbit) == 0 ]]; then ERR_CMD=/$(stat -c %U /opt/errbit)/.rbenv/shims/bundle; else ERR_CMD=/home/$(stat -c %U /opt/errbit)/.rbenv/shims/bundle; fi
  echo -e "[Unit]\nDescription=Errbit Server\nRequires=mongodb.service\nAfter=mongodb.service network.target\n\n[Service]\nWorkingDirectory=/opt/errbit\nExecStart=${ERR_CMD} exec rails server\nExecStop=/bin/kill -QUIT $MAINPID\nExecReload=/bin/kill -USR2 $MAINPID\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/errbit.service
  systemctl daemon-reload
  systemctl start errbit.service
  EOH
  end

service 'errbit' do
  action :enable
  retries 3
  end

